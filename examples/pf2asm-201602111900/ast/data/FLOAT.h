// $Id: FLOAT.h,v 1.1 2014/02/26 18:55:19 david Exp $
#ifndef __PF2ASM_NODE_data_FLOAT_H__
#define __PF2ASM_NODE_data_FLOAT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace data {

    class FLOAT: public cdk::basic_node {
      double _value;

    public:
      inline FLOAT(int lineno, float value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline double value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_FLOAT(this, level);
      }
    };

  } // data
} // pf2asm

#endif
