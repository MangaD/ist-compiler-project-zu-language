// $Id: UMOD.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_arithmetic_UMOD_H__
#define __PF2ASM_NODE_arithmetic_UMOD_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace arithmetic {

    class UMOD: public cdk::basic_node {
    public:
      inline UMOD(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_UMOD(this, level);
      }
    };

  } // arithmetic
} // pf2asm

#endif
