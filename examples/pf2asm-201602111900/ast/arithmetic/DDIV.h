// $Id: DDIV.h,v 1.1 2014/02/26 18:55:17 david Exp $
#ifndef __PF2ASM_NODE_arithmetic_DDIV_H__
#define __PF2ASM_NODE_arithmetic_DDIV_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace arithmetic {

    class DDIV: public cdk::basic_node {
    public:
      inline DDIV(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_DDIV(this, level);
      }
    };

  } // arithmetic
} // pf2asm

#endif
