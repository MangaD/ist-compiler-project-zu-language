// $Id: ADD.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_arithmetic_ADD_H__
#define __PF2ASM_NODE_arithmetic_ADD_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
    namespace arithmetic {

      class ADD: public cdk::basic_node {
      public:
        inline ADD(int lineno) :
          cdk::basic_node(lineno) {
        }
        inline void accept(basic_ast_visitor *sp, int level) {
          sp->do_ADD(this, level);
        }
      };

    } // arithmetic
} // pf2asm

#endif
