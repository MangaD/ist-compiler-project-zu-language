// $Id: LABEL.h,v 1.1 2014/02/26 18:55:27 david Exp $
#ifndef __PF2ASM_NODE_addressing_LABEL_H__
#define __PF2ASM_NODE_addressing_LABEL_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class LABEL: public cdk::basic_node {
      std::string _label;

    public:
      inline LABEL(int lineno, std::string &label) :
          cdk::basic_node(lineno), _label(label) {
      }
      inline const std::string &label() const {
        return _label;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LABEL(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
