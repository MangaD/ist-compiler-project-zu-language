// $Id: TEXT.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_TEXT_H__
#define __PF2ASM_NODE_addressing_TEXT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class TEXT: public cdk::basic_node {
    public:
      inline TEXT(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_TEXT(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
