// $Id: DATA.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_DATA_H__
#define __PF2ASM_NODE_addressing_DATA_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class DATA: public cdk::basic_node {
    public:
      inline DATA(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_DATA(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
