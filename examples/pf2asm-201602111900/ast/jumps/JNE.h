// $Id: JNE.h,v 1.1 2014/02/26 18:55:21 david Exp $
#ifndef __PF2ASM_NODE_jumps_JNE_H__
#define __PF2ASM_NODE_jumps_JNE_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace jumps {

    class JNE: public cdk::basic_node {
      std::string _label;

    public:
      inline JNE(int lineno, std::string &label) :
          cdk::basic_node(lineno), _label(label) {
      }
      inline const std::string &label() const {
        return _label;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_JNE(this, level);
      }
    };

  } // jumps
} // pf2asm

#endif
