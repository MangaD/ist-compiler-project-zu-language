// $Id: ROTL.h,v 1.1 2014/02/26 18:55:34 david Exp $
#ifndef __PF2ASM_NODE_bitwise_ROTL_H__
#define __PF2ASM_NODE_bitwise_ROTL_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace bitwise {

    class ROTL: public cdk::basic_node {
    public:
      inline ROTL(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ROTL(this, level);
      }
    };

  } // bitwise
} // pf2asm

#endif
