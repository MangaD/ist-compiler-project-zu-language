// $Id: SHTRU.h,v 1.1 2014/02/26 18:55:34 david Exp $
#ifndef __PF2ASM_NODE_bitwise_SHTRU_H__
#define __PF2ASM_NODE_bitwise_SHTRU_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace bitwise {

    class SHTRU: public cdk::basic_node {
    public:
      inline SHTRU(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_SHTRU(this, level);
      }
    };

  } // bitwise
} // pf2asm

#endif
