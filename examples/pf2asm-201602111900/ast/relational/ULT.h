// $Id: ULT.h,v 1.1 2014/02/26 18:55:32 david Exp $
#ifndef __PF2ASM_NODE_relational_ULT_H__
#define __PF2ASM_NODE_relational_ULT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace relational {

    class ULT: public cdk::basic_node {
    public:
      inline ULT(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ULT(this, level);
      }
    };

  } // relational
} // pf2asm

#endif
