// $Id: STORE.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_loadstore_STORE_H__
#define __PF2ASM_NODE_loadstore_STORE_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace loadstore {

    class STORE: public cdk::basic_node {
    public:
      inline STORE(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_STORE(this, level);
      }
    };

  } // loadstore
} // pf2asm

#endif
