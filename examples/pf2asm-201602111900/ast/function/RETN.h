// $Id: RETN.h,v 1.1 2014/02/26 18:55:14 david Exp $
#ifndef __PF2ASM_NODE_function_RETN_H__
#define __PF2ASM_NODE_function_RETN_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace function {

    class RETN: public cdk::basic_node {
      int _value;

    public:
      inline RETN(int lineno, int value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline int value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_RETN(this, level);
      }
    };

  } // function
} // pf2asm

#endif
