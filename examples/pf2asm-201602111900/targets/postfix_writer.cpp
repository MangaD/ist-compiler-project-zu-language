// $Id: postfix_writer.cpp,v 1.1 2014/02/26 18:55:26 david Exp $ -*- c++ -*-
#include <string>
#include <sstream>
#include "targets/postfix_writer.h"
#include "ast/all.h"

// must come after other #includes
#include "pf2asm_parser.tab.h"   /* token-based constants (from .y) */

//---------------------------------------------------------------------------
//          THIS IS THE POSTFIX CODE GENERATOR'S DEFINITION
//---------------------------------------------------------------------------

/**
 * A sequence in Postfix is simply a cycle through its items.
 * @param node the node representing the sequence.
 * @param lvl the syntax tree level
 */
void pf2asm::postfix_writer::do_sequence_node(cdk::sequence_node *const node, int lvl) {
  for (size_t i = 0; i < node->size(); i++) node->node(i)->accept(this, lvl);
}

//===========================================================================

void pf2asm::postfix_writer::do_NOP(pf2asm::NOP *node, int level) { _pf.NOP(); }
void pf2asm::postfix_writer::do_INCR(pf2asm::INCR *node, int level) { _pf.INCR(node->value()); }
void pf2asm::postfix_writer::do_DECR(pf2asm::DECR *node, int level) { _pf.DECR(node->value()); }
void pf2asm::postfix_writer::do_ALLOC(pf2asm::ALLOC *node, int level) { _pf.ALLOC(); }
void pf2asm::postfix_writer::do_TRASH(pf2asm::TRASH *node, int level) { _pf.TRASH(node->value()); }
void pf2asm::postfix_writer::do_DUP(pf2asm::DUP *node, int level) { _pf.DUP(); }
void pf2asm::postfix_writer::do_DDUP(pf2asm::DDUP *node, int level) { _pf.DDUP(); }
void pf2asm::postfix_writer::do_SWAP(pf2asm::SWAP *node, int level) { _pf.SWAP(); }
void pf2asm::postfix_writer::do_SP(pf2asm::SP *node, int level) { _pf.SP(); }
void pf2asm::postfix_writer::do_I2D(pf2asm::I2D *node, int level) { _pf.I2D(); }
void pf2asm::postfix_writer::do_F2D(pf2asm::F2D *node, int level) { _pf.F2D(); }
void pf2asm::postfix_writer::do_D2I(pf2asm::D2I *node, int level) { _pf.D2I(); }
void pf2asm::postfix_writer::do_D2F(pf2asm::D2F *node, int level) { _pf.D2F(); }
void pf2asm::postfix_writer::do_NIL(pf2asm::NIL *node, int level) { _pf.NIL(); }

void pf2asm::postfix_writer::do_CONST(pf2asm::data::CONST *node, int level) { _pf.CONST(node->value()); }
void pf2asm::postfix_writer::do_STR(pf2asm::data::STR *node, int level) { _pf.STR(node->value()); }
void pf2asm::postfix_writer::do_CHAR(pf2asm::data::CHAR *node, int level) { _pf.CHAR(node->value()[0]); }
void pf2asm::postfix_writer::do_ID(pf2asm::data::ID *node, int level) { _pf.ID(node->value()); }
void pf2asm::postfix_writer::do_BYTE(pf2asm::data::BYTE *node, int level) { _pf.BYTE(node->value()); }
void pf2asm::postfix_writer::do_INT(pf2asm::data::INT *node, int level) { _pf.INT(node->value()); }
void pf2asm::postfix_writer::do_FLOAT(pf2asm::data::FLOAT *node, int level) { _pf.FLOAT(node->value()); }
void pf2asm::postfix_writer::do_DOUBLE(pf2asm::data::DOUBLE *node, int level) { _pf.DOUBLE(node->value()); }

void pf2asm::postfix_writer::do_TEXT(pf2asm::addressing::TEXT *node, int level) { _pf.TEXT(); }
void pf2asm::postfix_writer::do_RODATA(pf2asm::addressing::RODATA *node, int level) { _pf.RODATA(); }
void pf2asm::postfix_writer::do_DATA(pf2asm::addressing::DATA *node, int level) { _pf.DATA(); }
void pf2asm::postfix_writer::do_BSS(pf2asm::addressing::BSS *node, int level) { _pf.BSS(); }
void pf2asm::postfix_writer::do_ALIGN(pf2asm::addressing::ALIGN *node, int level) { _pf.ALIGN(); }
void pf2asm::postfix_writer::do_EXTERN(pf2asm::addressing::EXTERN *node, int level) { _pf.EXTERN(node->label()); }
void pf2asm::postfix_writer::do_COMMON(pf2asm::addressing::COMMON *node, int level) { _pf.COMMON(node->value()); }
void pf2asm::postfix_writer::do_GLOBAL(pf2asm::addressing::GLOBAL *node, int level) {
  _pf.GLOBAL(node->label(), (node->type() == "FUNC" ? _pf.FUNC() : _pf.OBJ()));
}
void pf2asm::postfix_writer::do_LABEL(pf2asm::addressing::LABEL *node, int level) { _pf.LABEL(node->label()); }
void pf2asm::postfix_writer::do_LOCAL(pf2asm::addressing::LOCAL *node, int level) { _pf.LOCAL(node->offset()); }
void pf2asm::postfix_writer::do_ADDR(pf2asm::addressing::ADDR *node, int level) { _pf.ADDR(node->label()); }
void pf2asm::postfix_writer::do_LOCV(pf2asm::addressing::LOCV *node, int level) { _pf.LOCV(node->offset()); }
void pf2asm::postfix_writer::do_ADDRV(pf2asm::addressing::ADDRV *node, int level) { _pf.ADDRV(node->label()); }
void pf2asm::postfix_writer::do_LOCA(pf2asm::addressing::LOCA *node, int level) { _pf.LOCA(node->offset()); }
void pf2asm::postfix_writer::do_ADDRA(pf2asm::addressing::ADDRA *node, int level) { _pf.ADDRA(node->label()); }

void pf2asm::postfix_writer::do_ENTER(pf2asm::function::ENTER *node, int level) { _pf.ENTER(node->value()); }
void pf2asm::postfix_writer::do_START(pf2asm::function::START *node, int level) { _pf.START(); }
void pf2asm::postfix_writer::do_LEAVE(pf2asm::function::LEAVE *node, int level) { _pf.LEAVE(); }
void pf2asm::postfix_writer::do_CALL(pf2asm::function::CALL *node, int level) { _pf.CALL(node->label()); }
void pf2asm::postfix_writer::do_RET(pf2asm::function::RET *node, int level) { _pf.RET(); }
void pf2asm::postfix_writer::do_RETN(pf2asm::function::RETN *node, int level) { _pf.RETN(node->value()); }
void pf2asm::postfix_writer::do_PUSH(pf2asm::function::PUSH *node, int level) { _pf.PUSH(); }
void pf2asm::postfix_writer::do_POP(pf2asm::function::POP *node, int level) { _pf.POP(); }
void pf2asm::postfix_writer::do_DPUSH(pf2asm::function::DPUSH *node, int level) { _pf.DPUSH(); }
void pf2asm::postfix_writer::do_DPOP(pf2asm::function::DPOP *node, int level) { _pf.DPOP(); }

void pf2asm::postfix_writer::do_ULDCHR(pf2asm::loadstore::ULDCHR *node, int level) { _pf.ULDCHR(); }
void pf2asm::postfix_writer::do_ULD16(pf2asm::loadstore::ULD16 *node, int level) { _pf.ULD16(); }
void pf2asm::postfix_writer::do_LOAD(pf2asm::loadstore::LOAD *node, int level) { _pf.LOAD(); }
void pf2asm::postfix_writer::do_STORE(pf2asm::loadstore::STORE *node, int level) { _pf.STORE(); }
void pf2asm::postfix_writer::do_LDCHR(pf2asm::loadstore::LDCHR *node, int level) { _pf.LDCHR(); }
void pf2asm::postfix_writer::do_STCHR(pf2asm::loadstore::STCHR *node, int level) { _pf.STCHR(); }
void pf2asm::postfix_writer::do_LD16(pf2asm::loadstore::LD16 *node, int level) { _pf.LD16(); }
void pf2asm::postfix_writer::do_ST16(pf2asm::loadstore::ST16 *node, int level) { _pf.ST16(); }
void pf2asm::postfix_writer::do_DLOAD(pf2asm::loadstore::DLOAD *node, int level) { _pf.DLOAD(); }
void pf2asm::postfix_writer::do_DSTORE(pf2asm::loadstore::DSTORE *node, int level) { _pf.DSTORE(); }

void pf2asm::postfix_writer::do_ADD(pf2asm::arithmetic::ADD *node, int level) { _pf.ADD(); }
void pf2asm::postfix_writer::do_SUB(pf2asm::arithmetic::SUB *node, int level) { _pf.SUB(); }
void pf2asm::postfix_writer::do_MUL(pf2asm::arithmetic::MUL *node, int level) { _pf.MUL(); }
void pf2asm::postfix_writer::do_DIV(pf2asm::arithmetic::DIV *node, int level) { _pf.DIV(); }
void pf2asm::postfix_writer::do_MOD(pf2asm::arithmetic::MOD *node, int level) { _pf.MOD(); }
void pf2asm::postfix_writer::do_NEG(pf2asm::arithmetic::NEG *node, int level) { _pf.NEG(); }
void pf2asm::postfix_writer::do_UDIV(pf2asm::arithmetic::UDIV *node, int level) { _pf.UDIV(); }
void pf2asm::postfix_writer::do_UMOD(pf2asm::arithmetic::UMOD *node, int level) { _pf.UMOD(); }
void pf2asm::postfix_writer::do_DADD(pf2asm::arithmetic::DADD *node, int level) { _pf.DADD(); }
void pf2asm::postfix_writer::do_DSUB(pf2asm::arithmetic::DSUB *node, int level) { _pf.DSUB(); }
void pf2asm::postfix_writer::do_DMUL(pf2asm::arithmetic::DMUL *node, int level) { _pf.DMUL(); }
void pf2asm::postfix_writer::do_DDIV(pf2asm::arithmetic::DDIV *node, int level) { _pf.DDIV(); }
void pf2asm::postfix_writer::do_DNEG(pf2asm::arithmetic::DNEG *node, int level) { _pf.DNEG(); }

void pf2asm::postfix_writer::do_ROTL(pf2asm::bitwise::ROTL *node, int level) { _pf.ROTL(); }
void pf2asm::postfix_writer::do_ROTR(pf2asm::bitwise::ROTR *node, int level) { _pf.ROTR(); }
void pf2asm::postfix_writer::do_SHTL(pf2asm::bitwise::SHTL *node, int level) { _pf.SHTL(); }
void pf2asm::postfix_writer::do_SHTRU(pf2asm::bitwise::SHTRU *node, int level) { _pf.SHTRU(); }
void pf2asm::postfix_writer::do_SHTRS(pf2asm::bitwise::SHTRS *node, int level) { _pf.SHTRS(); }

void pf2asm::postfix_writer::do_AND(pf2asm::logical::AND *node, int level) { _pf.AND(); }
void pf2asm::postfix_writer::do_OR(pf2asm::logical::OR *node, int level) { _pf.OR(); }
void pf2asm::postfix_writer::do_XOR(pf2asm::logical::XOR *node, int level) { _pf.XOR(); }
void pf2asm::postfix_writer::do_NOT(pf2asm::logical::NOT *node, int level) { _pf.NOT(); }

void pf2asm::postfix_writer::do_GT(pf2asm::relational::GT *node, int level) { _pf.GT(); }
void pf2asm::postfix_writer::do_GE(pf2asm::relational::GE *node, int level) { _pf.GE(); }
void pf2asm::postfix_writer::do_LT(pf2asm::relational::LT *node, int level) { _pf.LT(); }
void pf2asm::postfix_writer::do_LE(pf2asm::relational::LE *node, int level) { _pf.LE(); }
void pf2asm::postfix_writer::do_EQ(pf2asm::relational::EQ *node, int level) { _pf.EQ(); }
void pf2asm::postfix_writer::do_NE(pf2asm::relational::NE *node, int level) { _pf.NE(); }
void pf2asm::postfix_writer::do_UGT(pf2asm::relational::UGT *node, int level) { _pf.UGT(); }
void pf2asm::postfix_writer::do_UGE(pf2asm::relational::UGE *node, int level) { _pf.UGE(); }
void pf2asm::postfix_writer::do_ULT(pf2asm::relational::ULT *node, int level) { _pf.ULT(); }
void pf2asm::postfix_writer::do_ULE(pf2asm::relational::ULE *node, int level) { _pf.ULE(); }
void pf2asm::postfix_writer::do_DCMP(pf2asm::relational::DCMP *node, int level) { _pf.DCMP(); }

void pf2asm::postfix_writer::do_BRANCH(pf2asm::jumps::BRANCH *node, int level) { _pf.BRANCH(); }
void pf2asm::postfix_writer::do_LEAP  (pf2asm::jumps::LEAP   *node, int level) { _pf.LEAP(); }
void pf2asm::postfix_writer::do_JMP   (pf2asm::jumps::JMP    *node, int level) { _pf.JMP(node->label()); }
void pf2asm::postfix_writer::do_JZ    (pf2asm::jumps::JZ     *node, int level) { _pf.JZ(node->label()); }
void pf2asm::postfix_writer::do_JNZ   (pf2asm::jumps::JNZ    *node, int level) { _pf.JNZ(node->label()); }
void pf2asm::postfix_writer::do_JEQ   (pf2asm::jumps::JEQ    *node, int level) { _pf.JEQ(node->label()); }
void pf2asm::postfix_writer::do_JNE   (pf2asm::jumps::JNE    *node, int level) { _pf.JNE(node->label()); }
void pf2asm::postfix_writer::do_JGT   (pf2asm::jumps::JGT    *node, int level) { _pf.JGT(node->label()); }
void pf2asm::postfix_writer::do_JGE   (pf2asm::jumps::JGE    *node, int level) { _pf.JGE(node->label()); }
void pf2asm::postfix_writer::do_JLT   (pf2asm::jumps::JLT    *node, int level) { _pf.JLT(node->label()); }
void pf2asm::postfix_writer::do_JLE   (pf2asm::jumps::JLE    *node, int level) { _pf.JLE(node->label()); }
void pf2asm::postfix_writer::do_JUGT  (pf2asm::jumps::JUGT   *node, int level) { _pf.JUGT(node->label()); }
void pf2asm::postfix_writer::do_JUGE  (pf2asm::jumps::JUGE   *node, int level) { _pf.JUGE(node->label()); }
void pf2asm::postfix_writer::do_JULT  (pf2asm::jumps::JULT   *node, int level) { _pf.JULT(node->label()); }
void pf2asm::postfix_writer::do_JULE  (pf2asm::jumps::JULE   *node, int level) { _pf.JULE(node->label()); }

//---------------------------------------------------------------------------
//     T H E    E N D
//---------------------------------------------------------------------------
