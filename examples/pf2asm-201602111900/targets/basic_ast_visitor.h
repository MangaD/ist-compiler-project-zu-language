// $Id: basic_ast_visitor.h,v 1.1 2014/02/26 18:55:26 david Exp $ -*- c++ -*-
#ifndef __PF2ASM_BASIC_AST_VISITOR_H__
#define __PF2ASM_BASIC_AST_VISITOR_H__

#include <string>
#include <iostream>
#include <cdk/compiler.h>

/* include node forward declarations */
#define __NODE_DECLARATIONS_ONLY__
#include "ast/all.h"  // automatically generated
#undef __NODE_DECLARATIONS_ONLY__

#define visig(nsp, nodeType) void do_##nodeType(nsp::nodeType *const node, int lvl)

//!
//! Print nodes as XML elements to the output stream.
//!
class basic_ast_visitor {
protected:
  //! The owner compiler
  std::shared_ptr<cdk::compiler> _compiler;

protected:
  basic_ast_visitor(std::shared_ptr<cdk::compiler> compiler) :
      _compiler(compiler) {
  }

  inline bool debug() {
    return _compiler->debug();
  }

  inline std::ostream &os() {
    return *_compiler->ostream();
  }

public:
  virtual ~basic_ast_visitor() {}

public:
  virtual void do_composite_node(cdk::composite_node * const node, int lvl) { throw 42; }
  virtual void do_data_node(cdk::data_node * const node, int lvl) { throw 42; }

  virtual void do_nil_node(cdk::nil_node * const node, int lvl) = 0;
  virtual void do_sequence_node(cdk::sequence_node * const node, int lvl) = 0;

public:
  virtual void do_double_node(cdk::double_node * const node, int lvl) { throw 42; }
  virtual void do_integer_node(cdk::integer_node * const node, int lvl) { throw 42; }
  virtual void do_string_node(cdk::string_node * const node, int lvl) { throw 42; }
  virtual void do_identifier_node(cdk::identifier_node * const node, int lvl) { throw 42; }

public:
  virtual void do_neg_node(cdk::neg_node * const node, int lvl) { throw 42; }
  virtual void do_add_node(cdk::add_node * const node, int lvl) { throw 42; }
  virtual void do_div_node(cdk::div_node * const node, int lvl) { throw 42; }
  virtual void do_mod_node(cdk::mod_node * const node, int lvl) { throw 42; }
  virtual void do_mul_node(cdk::mul_node * const node, int lvl) { throw 42; }
  virtual void do_sub_node(cdk::sub_node * const node, int lvl) { throw 42; }

public:
  virtual void do_eq_node(cdk::eq_node * const node, int lvl) { throw 42; }
  virtual void do_ge_node(cdk::ge_node * const node, int lvl) { throw 42; }
  virtual void do_gt_node(cdk::gt_node * const node, int lvl) { throw 42; }
  virtual void do_le_node(cdk::le_node * const node, int lvl) { throw 42; }
  virtual void do_lt_node(cdk::lt_node * const node, int lvl) { throw 42; }
  virtual void do_ne_node(cdk::ne_node * const node, int lvl) { throw 42; }

public:
  // postfix keyword nodes
  virtual void do_NOP(pf2asm::NOP * const node, int lvl) = 0;
  virtual void do_INCR(pf2asm::INCR * const node, int lvl) = 0;
  virtual void do_DECR(pf2asm::DECR * const node, int lvl) = 0;
  virtual void do_ALLOC(pf2asm::ALLOC * const node, int lvl) = 0;
  virtual void do_TRASH(pf2asm::TRASH * const node, int lvl) = 0;
  virtual void do_DUP(pf2asm::DUP * const node, int lvl) = 0;
  virtual void do_DDUP(pf2asm::DDUP * const node, int lvl) = 0;
  virtual void do_SWAP(pf2asm::SWAP * const node, int lvl) = 0;
  virtual void do_SP(pf2asm::SP * const node, int lvl) = 0;
  virtual void do_I2D(pf2asm::I2D * const node, int lvl) = 0;
  virtual void do_F2D(pf2asm::F2D * const node, int lvl) = 0;
  virtual void do_D2I(pf2asm::D2I * const node, int lvl) = 0;
  virtual void do_D2F(pf2asm::D2F * const node, int lvl) = 0;
  virtual void do_NIL(pf2asm::NIL * const node, int lvl) = 0;

  virtual void do_CONST(pf2asm::data::CONST * const node, int lvl) = 0;
  virtual void do_STR(pf2asm::data::STR * const node, int lvl) = 0;
  virtual void do_CHAR(pf2asm::data::CHAR * const node, int lvl) = 0;
  virtual void do_ID(pf2asm::data::ID * const node, int lvl) = 0;
  virtual void do_BYTE(pf2asm::data::BYTE * const node, int lvl) = 0;
  virtual void do_INT(pf2asm::data::INT * const node, int lvl) = 0;
  virtual void do_FLOAT(pf2asm::data::FLOAT * const node, int lvl) = 0;
  virtual void do_DOUBLE(pf2asm::data::DOUBLE * const node, int lvl) = 0;

  virtual void do_TEXT(pf2asm::addressing::TEXT * const node, int lvl) = 0;
  virtual void do_RODATA(pf2asm::addressing::RODATA * const node, int lvl) = 0;
  virtual void do_DATA(pf2asm::addressing::DATA * const node, int lvl) = 0;
  virtual void do_BSS(pf2asm::addressing::BSS * const node, int lvl) = 0;
  virtual void do_ALIGN(pf2asm::addressing::ALIGN * const node, int lvl) = 0;
  virtual void do_EXTERN(pf2asm::addressing::EXTERN * const node, int lvl) = 0;
  virtual void do_COMMON(pf2asm::addressing::COMMON * const node, int lvl) = 0;
  virtual void do_GLOBAL(pf2asm::addressing::GLOBAL * const node, int lvl) = 0;
  virtual void do_LABEL(pf2asm::addressing::LABEL * const node, int lvl) = 0;
  virtual void do_LOCAL(pf2asm::addressing::LOCAL * const node, int lvl) = 0;
  virtual void do_ADDR(pf2asm::addressing::ADDR * const node, int lvl) = 0;
  virtual void do_LOCV(pf2asm::addressing::LOCV * const node, int lvl) = 0;
  virtual void do_ADDRV(pf2asm::addressing::ADDRV * const node, int lvl) = 0;
  virtual void do_LOCA(pf2asm::addressing::LOCA * const node, int lvl) = 0;
  virtual void do_ADDRA(pf2asm::addressing::ADDRA * const node, int lvl) = 0;

  virtual void do_ENTER(pf2asm::function::ENTER * const node, int lvl) = 0;
  virtual void do_START(pf2asm::function::START * const node, int lvl) = 0;
  virtual void do_LEAVE(pf2asm::function::LEAVE * const node, int lvl) = 0;
  virtual void do_CALL(pf2asm::function::CALL * const node, int lvl) = 0;
  virtual void do_RET(pf2asm::function::RET * const node, int lvl) = 0;
  virtual void do_RETN(pf2asm::function::RETN * const node, int lvl) = 0;
  virtual void do_PUSH(pf2asm::function::PUSH * const node, int lvl) = 0;
  virtual void do_POP(pf2asm::function::POP * const node, int lvl) = 0;
  virtual void do_DPUSH(pf2asm::function::DPUSH * const node, int lvl) = 0;
  virtual void do_DPOP(pf2asm::function::DPOP * const node, int lvl) = 0;

  virtual void do_ULDCHR(pf2asm::loadstore::ULDCHR * const node, int lvl) = 0;
  virtual void do_ULD16(pf2asm::loadstore::ULD16 * const node, int lvl) = 0;
  virtual void do_LOAD(pf2asm::loadstore::LOAD * const node, int lvl) = 0;
  virtual void do_STORE(pf2asm::loadstore::STORE * const node, int lvl) = 0;
  virtual void do_LDCHR(pf2asm::loadstore::LDCHR * const node, int lvl) = 0;
  virtual void do_STCHR(pf2asm::loadstore::STCHR * const node, int lvl) = 0;
  virtual void do_LD16(pf2asm::loadstore::LD16 * const node, int lvl) = 0;
  virtual void do_ST16(pf2asm::loadstore::ST16 * const node, int lvl) = 0;
  virtual void do_DLOAD(pf2asm::loadstore::DLOAD * const node, int lvl) = 0;
  virtual void do_DSTORE(pf2asm::loadstore::DSTORE * const node, int lvl) = 0;

  virtual void do_ADD(pf2asm::arithmetic::ADD * const node, int lvl) = 0;
  virtual void do_SUB(pf2asm::arithmetic::SUB * const node, int lvl) = 0;
  virtual void do_MUL(pf2asm::arithmetic::MUL * const node, int lvl) = 0;
  virtual void do_DIV(pf2asm::arithmetic::DIV * const node, int lvl) = 0;
  virtual void do_MOD(pf2asm::arithmetic::MOD * const node, int lvl) = 0;
  virtual void do_NEG(pf2asm::arithmetic::NEG * const node, int lvl) = 0;
  virtual void do_UDIV(pf2asm::arithmetic::UDIV * const node, int lvl) = 0;
  virtual void do_UMOD(pf2asm::arithmetic::UMOD * const node, int lvl) = 0;
  virtual void do_DADD(pf2asm::arithmetic::DADD * const node, int lvl) = 0;
  virtual void do_DSUB(pf2asm::arithmetic::DSUB * const node, int lvl) = 0;
  virtual void do_DMUL(pf2asm::arithmetic::DMUL * const node, int lvl) = 0;
  virtual void do_DDIV(pf2asm::arithmetic::DDIV * const node, int lvl) = 0;
  virtual void do_DNEG(pf2asm::arithmetic::DNEG * const node, int lvl) = 0;

  virtual void do_ROTL(pf2asm::bitwise::ROTL * const node, int lvl) = 0;
  virtual void do_ROTR(pf2asm::bitwise::ROTR * const node, int lvl) = 0;
  virtual void do_SHTL(pf2asm::bitwise::SHTL * const node, int lvl) = 0;
  virtual void do_SHTRU(pf2asm::bitwise::SHTRU * const node, int lvl) = 0;
  virtual void do_SHTRS(pf2asm::bitwise::SHTRS * const node, int lvl) = 0;

  virtual void do_AND(pf2asm::logical::AND * const node, int lvl) = 0;
  virtual void do_OR(pf2asm::logical::OR * const node, int lvl) = 0;
  virtual void do_XOR(pf2asm::logical::XOR * const node, int lvl) = 0;
  virtual void do_NOT(pf2asm::logical::NOT * const node, int lvl) = 0;

  virtual void do_GT(pf2asm::relational::GT * const node, int lvl) = 0;
  virtual void do_GE(pf2asm::relational::GE * const node, int lvl) = 0;
  virtual void do_LT(pf2asm::relational::LT * const node, int lvl) = 0;
  virtual void do_LE(pf2asm::relational::LE * const node, int lvl) = 0;
  virtual void do_EQ(pf2asm::relational::EQ * const node, int lvl) = 0;
  virtual void do_NE(pf2asm::relational::NE * const node, int lvl) = 0;
  virtual void do_UGT(pf2asm::relational::UGT * const node, int lvl) = 0;
  virtual void do_UGE(pf2asm::relational::UGE * const node, int lvl) = 0;
  virtual void do_ULT(pf2asm::relational::ULT * const node, int lvl) = 0;
  virtual void do_ULE(pf2asm::relational::ULE * const node, int lvl) = 0;
  virtual void do_DCMP(pf2asm::relational::DCMP * const node, int lvl) = 0;

  virtual void do_BRANCH(pf2asm::jumps::BRANCH * const node, int lvl) = 0;
  virtual void do_LEAP(pf2asm::jumps::LEAP * const node, int lvl) = 0;
  virtual void do_JMP(pf2asm::jumps::JMP * const node, int lvl) = 0;
  virtual void do_JZ(pf2asm::jumps::JZ * const node, int lvl) = 0;
  virtual void do_JNZ(pf2asm::jumps::JNZ * const node, int lvl) = 0;
  virtual void do_JEQ(pf2asm::jumps::JEQ * const node, int lvl) = 0;
  virtual void do_JNE(pf2asm::jumps::JNE * const node, int lvl) = 0;
  virtual void do_JGT(pf2asm::jumps::JGT * const node, int lvl) = 0;
  virtual void do_JGE(pf2asm::jumps::JGE * const node, int lvl) = 0;
  virtual void do_JLT(pf2asm::jumps::JLT * const node, int lvl) = 0;
  virtual void do_JLE(pf2asm::jumps::JLE * const node, int lvl) = 0;
  virtual void do_JUGT(pf2asm::jumps::JUGT * const node, int lvl) = 0;
  virtual void do_JUGE(pf2asm::jumps::JUGE * const node, int lvl) = 0;
  virtual void do_JULT(pf2asm::jumps::JULT * const node, int lvl) = 0;
  virtual void do_JULE(pf2asm::jumps::JULE * const node, int lvl) = 0;

};

#endif
