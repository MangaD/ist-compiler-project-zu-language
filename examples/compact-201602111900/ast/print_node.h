// $Id: print_node.h,v 1.4 2014/03/31 15:52:10 david Exp $ -*- c++ -*-
#ifndef __COMPACT_PRINTNODE_H__
#define __COMPACT_PRINTNODE_H__

#include <cdk/ast/expression_node.h>

namespace compact {

  /**
   * Class for describing print nodes.
   */
  class print_node: public cdk::basic_node {
    cdk::expression_node *_argument;

  public:
    inline print_node(int lineno, cdk::expression_node *argument) :
        cdk::basic_node(lineno), _argument(argument) {
    }

  public:
    inline cdk::expression_node *argument() {
      return _argument;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_print_node(this, level);
    }

  };

} // compact

#endif
