// $Id: interpreter.cpp,v 1.3 2014/03/31 15:52:10 david Exp $ -*- c++ -*-
#include <string>
#include "targets/interpreter.h"
#include "ast/all.h"  // automatically generated

//---------------------------------------------------------------------------

void compact::interpreter::do_sequence_node(cdk::sequence_node * const node, int lvl) {
  for (size_t i = 0; i < node->size(); i++)
    node->node(i)->accept(this, lvl + 2);
}

//---------------------------------------------------------------------------

void compact::interpreter::do_integer_node(cdk::integer_node * const node, int lvl) {
  _stack.push(node->value());
}

void compact::interpreter::do_double_node(cdk::double_node * const node, int lvl) {
}

void compact::interpreter::do_string_node(cdk::string_node * const node, int lvl) {
  os() << node->value() << std::endl;
}

//---------------------------------------------------------------------------

void compact::interpreter::do_identifier_node(cdk::identifier_node * const node, int lvl) {
  const std::string &id = node->value();
  std::shared_ptr < compact::symbol > symbol = _symtab.find(id);
  _stack.push(symbol->value());
}

//---------------------------------------------------------------------------

void compact::interpreter::do_neg_node(cdk::neg_node * const node, int lvl) {
  node->argument()->accept(this, lvl);
  _stack.push(-_stack.pop());
}

//---------------------------------------------------------------------------

void compact::interpreter::do_add_node(cdk::add_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() + _stack.pop());
}
void compact::interpreter::do_sub_node(cdk::sub_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() - _stack.pop());
}
void compact::interpreter::do_mul_node(cdk::mul_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() * _stack.pop());
}
void compact::interpreter::do_div_node(cdk::div_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() / _stack.pop());
}
void compact::interpreter::do_mod_node(cdk::mod_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() % _stack.pop());
}
void compact::interpreter::do_lt_node(cdk::lt_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() < _stack.pop());
}
void compact::interpreter::do_le_node(cdk::le_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() <= _stack.pop());
}
void compact::interpreter::do_ge_node(cdk::ge_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() >= _stack.pop());
}
void compact::interpreter::do_gt_node(cdk::gt_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() > _stack.pop());
}
void compact::interpreter::do_ne_node(cdk::ne_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() != _stack.pop());
}
void compact::interpreter::do_eq_node(cdk::eq_node * const node, int lvl) {
  node->right()->accept(this, lvl);
  node->left()->accept(this, lvl);
  _stack.push(_stack.pop() == _stack.pop());
}

//---------------------------------------------------------------------------

void compact::interpreter::do_lvalue_node(compact::lvalue_node * const node, int lvl) {
  const std::string &id = node->value();
  std::shared_ptr < compact::symbol > symbol = _symtab.find(id);
  _stack.push(symbol->value());
}

//---------------------------------------------------------------------------

void compact::interpreter::do_program_node(compact::program_node * const node, int lvl) {
  node->statements()->accept(this, lvl);
}

//---------------------------------------------------------------------------

void compact::interpreter::do_print_node(compact::print_node * const node, int lvl) {
  node->argument()->accept(this, lvl);
  os() << _stack.pop() << std::endl;
}

//---------------------------------------------------------------------------

void compact::interpreter::do_read_node(compact::read_node * const node, int lvl) {
  const std::string &id = node->argument()->value();
  std::shared_ptr < compact::symbol > symbol = _symtab.find(id);
  if (symbol != nullptr) {
    int val;
    std::cin >> val;
    symbol->value(val);
  }
}

void compact::interpreter::do_assignment_node(compact::assignment_node * const node, int lvl) {
  const std::string &id = node->lvalue()->value();
  std::shared_ptr < compact::symbol > symbol = _symtab.find(id);
  if (symbol == nullptr) {
    symbol = std::make_shared < compact::symbol > (0, id, 0);
    _symtab.insert(id, symbol);
    symbol = _symtab.find(id);
  }
  node->rvalue()->accept(this, lvl);
  symbol->value(_stack.pop());
}

//---------------------------------------------------------------------------

void compact::interpreter::do_while_node(compact::while_node * const node, int lvl) {
  node->condition()->accept(this, lvl);
  while (_stack.pop()) {
    node->block()->accept(this, lvl);
    node->condition()->accept(this, lvl);
  }
}

//---------------------------------------------------------------------------

void compact::interpreter::do_if_node(compact::if_node * const node, int lvl) {
  node->condition()->accept(this, lvl);
  if (_stack.pop())
    node->block()->accept(this, lvl);
}

void compact::interpreter::do_if_else_node(compact::if_else_node * const node, int lvl) {
  node->condition()->accept(this, lvl);
  if (_stack.pop())
    node->thenblock()->accept(this, lvl);
  else
    node->elseblock()->accept(this, lvl);
}
