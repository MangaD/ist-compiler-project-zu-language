// $Id: english_target.h,v 1.3 2014/03/31 15:52:10 david Exp $
#ifndef __COMPACT_SEMANTICS_ENGLISH_EVALUATOR_H__
#define __COMPACT_SEMANTICS_ENGLISH_EVALUATOR_H__

#include <cdk/basic_target.h>
#include <cdk/ast/basic_node.h>
#include <cdk/compiler.h>
#include "targets/english_writer.h"

namespace compact {

  class english_target: public cdk::basic_target {
    static english_target _self;

  private:
    inline english_target() :
        cdk::basic_target("english") {
    }

  public:
    bool evaluate(std::shared_ptr<cdk::compiler> compiler) {
      english_writer writer(compiler);
      compiler->ast()->accept(&writer, 0);
      return true;
    }

  };

} // compact

#endif
