// $Id: compact_scanner.h,v 1.2 2014/02/25 21:42:29 david Exp $ -*- c++ -*-
#ifndef __COMPACTSCANNER_H__
#define __COMPACTSCANNER_H__

#undef yyFlexLexer
#define yyFlexLexer compact_scanner
#include <FlexLexer.h>

#endif
