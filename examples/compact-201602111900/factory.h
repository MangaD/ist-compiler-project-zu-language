// $Id: factory.h,v 1.4 2014/03/31 15:52:09 david Exp $ -*- c++ -*-
#ifndef __COMPACT_FACTORY_H__
#define __COMPACT_FACTORY_H__

#include <memory>
#include <cdk/yy_factory.h>
#include "compact_scanner.h"

namespace compact {

  /**
   * This class implements the compiler factory for the Compact compiler.
   */
  class factory: public cdk::yy_factory<compact_scanner> {
    /**
     * This object is automatically registered by the constructor in the
     * superclass' language registry.
     */
    static factory _self;

  protected:
    /**
     * @param language name of the language handled by this factory (see .cpp file)
     */
    factory(const std::string &language = "compact") :
        cdk::yy_factory<compact_scanner>(language) {
    }

  };

} // compact

#endif
