#ifndef __ZU_FUNCTIONDEFINITIONNODE_H__
#define __ZU_FUNCTIONDEFINITIONNODE_H__

#include <string>
#include <cdk/ast/basic_node.h>
#include <cdk/ast/sequence_node.h>
#include <cdk/basic_type.h>
#include "ast/lvalue_node.h"
#include "ast/block_node.h"

namespace zu {

  /**
   * Class for describing function definition
   */
   class function_definition_node: public cdk::basic_node {
     // This must be a pointer, so that we can anchor a dynamic
     // object and be able to change/delete it afterwards.
     basic_type *_type;
     std::string *_id;
     cdk::sequence_node *_args;
     zu::lvalue_node *_lvalue;//opcional
     zu::block_node *_block;

   public:
    inline function_definition_node(int lineno, basic_type *type, std::string *id,
                                     cdk::sequence_node *_args, zu::lvalue_node *lvalue,
                                     zu::block_node *block) :
            cdk::basic_node(lineno), _type(type), _id(id), _lvalue(lvalue), _block(block) {
    }

   public:
    inline basic_type *type() {
        return _type;
    }

    inline std::string *id() {
        return _id;
    }

    inline cdk::sequence_node *args() {
        return _args;
    }

    inline zu::lvalue_node *lvalue() {
        return _lvalue;
    }

    inline zu::block_node *block() {
        return _block;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_function_definition_node(this, level);
    }

   };

} // zu

#endif
