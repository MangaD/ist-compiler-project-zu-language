#ifndef __ZU_ADDRESSNODE_H__
#define __ZU_ADDRESSNODE_H__

#include <cdk/ast/unary_expression_node.h>

namespace zu {

  /**
   * Class for describing address operator.
   */
   class address_node: public cdk::unary_expression_node {

   public:
    inline address_node(int lineno, cdk::unary_expression_node *arg) :
            cdk::unary_expression_node(lineno, arg) {
    }

   public:
    void accept(basic_ast_visitor *sp, int level) {
      sp->do_address_node(this, level);
    }

   };

} // zu

#endif
