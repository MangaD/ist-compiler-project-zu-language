#ifndef __ZU_BLOCKNODE_H__
#define __ZU_BLOCKNODE_H__

#include <cdk/ast/basic_node.h>

namespace zu {

  /**
   * Class for describing a block.
   */
   class block_node: public cdk::basic_node {
     // This must be a pointer, so that we can anchor a dynamic
     // object and be able to change/delete it afterwards.
     cdk::basic_node *_decls;
     cdk::basic_node *_statements;

   public:
    inline block_node(int lineno, cdk::basic_node *decls, cdk::basic_node *statements) :
            cdk::basic_node(lineno), _decls(decls), _statements(statements) {
    }

   public:
    inline cdk::basic_node *decls() {
        return _decls;
    }

    inline cdk::basic_node *statements() {
        return _statements;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_block_node(this, level);
    }

   };

} // zu

#endif
