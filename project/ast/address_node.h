#ifndef __ZU_ADDRESSNODE_H__
#define __ZU_ADDRESSNODE_H__

#include <cdk/ast/unary_expression_node.h>

namespace zu {

  /**
   * Class for describing address operator.
   */
   class address_node: public cdk::expression_node {

     zu::lvalue_node *_lvalue;

   public:
    inline address_node(int lineno, zu::lvalue_node *lvalue) :
            cdk::expression_node(lineno), _lvalue(lvalue) {
    }

   public:

    inline zu::lvalue_node *lvalue() {
        return _lvalue;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_address_node(this, level);
    }

   };

} // zu

#endif
