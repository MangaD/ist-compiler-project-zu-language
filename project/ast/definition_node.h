#ifndef __ZU_DEFINITIONNODE_H__
#define __ZU_DEFINITIONNODE_H__

#include <string>
#include <cdk/ast/expression_node.h>
#include <cdk/basic_type.h>

namespace zu {

  /**
   * Class for describing defining variables.
   */
   class definition_node : public cdk::basic_node {
     // This must be a pointer, so that we can anchor a dynamic
     // object and be able to change/delete it afterwards.
     basic_type *_type;
     std::string *_id;
     cdk::expression_node *_rvalue;

   public:
    inline definition_node(int lineno, basic_type *type, std::string *id,
                           cdk::expression_node *rvalue) :
            basic_node(lineno), _type(type), _id(id), _rvalue(rvalue) {
    }

   public:

    inline basic_type *type() {
        return _type;
    }

    inline std::string *id() {
        return _id;
    }

    inline cdk::expression_node *rvalue() {
        return _rvalue;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_definition_node(this, level);
    }

   };

} // zu

#endif
