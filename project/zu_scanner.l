%option c++ yyclass="zu_scanner" outfile="zu_scanner.cpp"
%option stack noyywrap yylineno 8bit
%{
/* $Id: zu_scanner.l,v 1.13 2016/05/19 20:42:37 ist173891 Exp $ */
// make relevant includes before including the parser's tab file
#include <string>
#include <sstream>
#include <cstdlib>
#include <cstdint>//For int32 max/min
#include <limits>//For int32 max/min
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/expression_node.h>
#include "zu_scanner.h"
#include "zu_parser.tab.h"

// don't change this
#define yyerror LexerError
%}
%x X_STRING X_MCOMMENT X_STRINGNULO

DIGIT                  [0-9]
HEXDIGIT               [0-9A-Fa-f]
INTEGER                (([1-9]{DIGIT}*)|0)
EXPONENT               ([eE][+-]?{INTEGER})
REAL                   ({INTEGER}\.{INTEGER}{EXPONENT}?)|({INTEGER}{EXPONENT})
HEX                    0x{HEXDIGIT}+
ID                     [A-Za-z_][A-Za-z0-9_]*
%%

"//".*                 ; /* ignore comments */

"/*"                   yy_push_state(X_MCOMMENT);
<X_MCOMMENT>"/*"       yy_push_state(X_MCOMMENT);
<X_MCOMMENT>"*/"       yy_pop_state();
<X_MCOMMENT><<EOF>>    yyerror( std::string("Line: " + std::to_string(yylineno) + ":Error: Unclosed comment.").c_str());
<X_MCOMMENT>.|\n       ; /* ignora*/

\"                     { yy_push_state(X_STRING); yylval.s = new std::string(""); }
<X_STRING>\"           { yy_pop_state(); return tSTRING; }
<X_STRING>\\n          { *yylval.s += '\n'; }
<X_STRING>\\r          { *yylval.s += '\r'; }
<X_STRING>\\t          { *yylval.s += '\t'; }
<X_STRING>\\\"         { *yylval.s += '\"'; }
<X_STRING>\\\\         { *yylval.s += "\\"; }
<X_STRING>\\{HEXDIGIT}{HEXDIGIT}? { if(std::string(&yytext[1]) == "0"){ yy_push_state(X_STRINGNULO); return tSTRING; } else{ long int i = std::strtol(&yytext[1], NULL, 16); *yylval.s += static_cast<char>(i); } }
<X_STRING><<EOF>>      yyerror( std::string("Line: " + std::to_string(yylineno) + ":Error: Unclosed string.").c_str());
<X_STRING>.            *yylval.s += yytext;
<X_STRING>\n           yyerror( std::string("Line: " + std::to_string(yylineno) + ": Error: Newline in string.").c_str() );
<X_STRING>\\.          yyerror( std::string("Line: " + std::to_string(yylineno) + ": Error: Unrecognized escape character " + "'" + &yytext[1] + "'.").c_str() );

<X_STRINGNULO>\"       {yy_pop_state(); yy_pop_state();}
<X_STRINGNULO>\n       yyerror( std::string("Line: " + std::to_string(yylineno) + ": Error: Newline in string.").c_str() );
<X_STRINGNULO><<EOF>>  yyerror( std::string("Line: " + std::to_string(yylineno) + ":Error Unclosed string.").c_str());
<X_STRINGNULO>.        ;
<X_STRINGNULO>\\.      yyerror( std::string("Line: " + std::to_string(yylineno) + ": Error: Unrecognized escape character " + "'" + &yytext[1] + "'.").c_str() );

">="                   return tGE;
"<="                   return tLE;
"=="                   return tEQ;
"!="                   return tNE;

"~"                    return tNO;
"&"                    return tAND;
"|"                    return tOR;

"<>"                   return tCONTINUE;
"><"                   return tBREAK;
"!!!"                  return tRETURN;


{ID}                   { yylval.s = new std::string(yytext); return tIDENTIFIER; }
{INTEGER}              { long i = std::strtol(yytext, nullptr, 10); if(i > std::numeric_limits<std::int32_t>::max() || i < std::numeric_limits<std::int32_t>::min()){ yyerror( std::string("Line: " + std::to_string(yylineno) + ":Integer overflow.").c_str()); } yylval.i = i; return tINTEGER; }
{HEX}                  { long i = std::strtol(yytext, nullptr, 16); if(i > std::numeric_limits<std::int32_t>::max() || i < std::numeric_limits<std::int32_t>::min()){ yyerror( std::string("Line: " + std::to_string(yylineno) + ":Integer overflow.").c_str()); } yylval.i = i; return tINTEGER; }
{REAL}                 { std::stringstream myStream(yytext); myStream >> yylval.d; return tREAL; }

[-()<>=+*\/%;:{}.,!?\[\]$@#]  return *yytext;

[ \t\n\r]+               ; /* ignore whitespace */

.                      yyerror(std::string("Line: " + std::to_string(yylineno) + ":Error: Unknown character '" + yytext + "'.").c_str());

%%
// Very, very dirty hack: flex is a mess generating C++ scanners.
int zu_scanner::yywrap() { return 1; }
