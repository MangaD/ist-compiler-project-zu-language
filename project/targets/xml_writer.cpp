// $Id: xml_writer.cpp,v 1.25 2016/05/20 15:26:15 ist173891 Exp $ -*- c++ -*-
#include <string>
#include "targets/xml_writer.h"
#include "targets/type_checker.h"
#include "ast/all.h"  // automatically generated

//---------------------------------------------------------------------------

void zu::xml_writer::do_sequence_node(cdk::sequence_node * const node, int lvl) {
  os() << std::string(lvl, ' ') << "<sequence_node size='" << node->size() << "'>" << std::endl;
  for (size_t i = 0; i < node->size(); i++)
    node->node(i)->accept(this, lvl + 2);
  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_integer_node(cdk::integer_node * const node, int lvl) {
  processSimple(node, lvl);
}

void zu::xml_writer::do_string_node(cdk::string_node * const node, int lvl) {
  processSimple(node, lvl);
}

//---------------------------------------------------------------------------

inline void zu::xml_writer::processUnaryExpression(cdk::unary_expression_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  node->argument()->accept(this, lvl + 2);
  closeTag(node, lvl);
}

void zu::xml_writer::do_neg_node(cdk::neg_node * const node, int lvl) {
  processUnaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

inline void zu::xml_writer::processBinaryExpression(cdk::binary_expression_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  node->left()->accept(this, lvl + 2);
  node->right()->accept(this, lvl + 2);
  closeTag(node, lvl);
}

void zu::xml_writer::do_add_node(cdk::add_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_sub_node(cdk::sub_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_mul_node(cdk::mul_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_div_node(cdk::div_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_mod_node(cdk::mod_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_lt_node(cdk::lt_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_le_node(cdk::le_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_ge_node(cdk::ge_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_gt_node(cdk::gt_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_ne_node(cdk::ne_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}
void zu::xml_writer::do_eq_node(cdk::eq_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_rvalue_node(zu::rvalue_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  node->lvalue()->accept(this, lvl + 4);
  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_lvalue_node(zu::lvalue_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  processSimple(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_assignment_node(zu::assignment_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  node->lvalue()->accept(this, lvl + 2);
  openTag("rvalue", lvl + 2);
  node->rvalue()->accept(this, lvl + 4);
  closeTag("rvalue", lvl + 2);
  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_evaluation_node(zu::evaluation_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  if(node->argument() != nullptr)
      node->argument()->accept(this, lvl + 2);
  closeTag(node, lvl);
}

void zu::xml_writer::do_print_node(zu::print_node * const node, int lvl) {
  CHECK_TYPES(_compiler, _symtab, node);
  openTag(node, lvl);
  if(node->argument() != nullptr)
      node->argument()->accept(this, lvl + 2);
  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_read_node(zu::read_node * const node, int lvl) {
    os() << std::string(lvl, ' ') << "<" << node->name() << " />" << std::endl;
  //openTag(node, lvl);
//FIXME  node->argument()->accept(this, lvl + 2);
  //closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_for_node(zu::for_node * const node, int lvl) {
  openTag(node, lvl);

  openTag("init", lvl + 2);
  if(node->init() != nullptr)
      node->init()->accept(this, lvl + 4);
  closeTag("init", lvl + 2);

  openTag("condition", lvl + 2);
  if(node->condition() != nullptr)
      node->condition()->accept(this, lvl + 4);
  closeTag("condition", lvl + 2);

  openTag("incr", lvl + 2);
  if(node->incr() != nullptr)
      node->incr()->accept(this, lvl + 4);
  closeTag("incr", lvl + 2);

  openTag("block", lvl + 2);
  node->block()->accept(this, lvl + 4);
  closeTag("block", lvl + 2);

  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_if_node(zu::if_node * const node, int lvl) {
  openTag(node, lvl);
  openTag("condition", lvl + 2);
  node->condition()->accept(this, lvl + 4);
  closeTag("condition", lvl + 2);
  openTag("then", lvl + 2);
  node->block()->accept(this, lvl + 4);
  closeTag("then", lvl + 2);
  closeTag(node, lvl);
}

void zu::xml_writer::do_if_else_node(zu::if_else_node * const node, int lvl) {
  openTag(node, lvl);
  openTag("condition", lvl + 2);
  node->condition()->accept(this, lvl + 4);
  closeTag("condition", lvl + 2);
  openTag("then", lvl + 2);
  node->thenblock()->accept(this, lvl + 4);
  closeTag("then", lvl + 2);
  openTag("else", lvl + 2);
  node->elseblock()->accept(this, lvl + 4);
  closeTag("else", lvl + 2);
  closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_declaration_node(zu::declaration_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("type", lvl + 2);
    processType(node->type(), lvl + 4);//FIXME
    closeTag("type", lvl + 2);
    openTag("id", lvl + 2);
    os() <<  std::string(lvl + 4, ' ') << *node->id() << std::endl;//FIXME
    closeTag("id", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_definition_node(zu::definition_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("type", lvl + 2);
    processType(node->type(), lvl + 4);//FIXME
    closeTag("type", lvl + 2);
    openTag("id", lvl + 2);
    os() <<  std::string(lvl + 4, ' ') << *node->id() << std::endl;//FIXME
    closeTag("id", lvl + 2);
    openTag("rvalue", lvl + 2);
    node->rvalue()->accept(this, lvl + 4);
    closeTag("rvalue", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_block_node(zu::block_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("decls", lvl + 2);
    if(node->decls() != nullptr)
        node->decls()->accept(this, lvl + 4);
    closeTag("decls", lvl + 2);
    openTag("statements", lvl + 2);
    if(node->statements() != nullptr)
        node->statements()->accept(this, lvl + 4);
    closeTag("statements", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_and_node(zu::and_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_or_node(zu::or_node * const node, int lvl) {
  processBinaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_not_node(zu::not_node * const node, int lvl) {
  processUnaryExpression(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_address_node(zu::address_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("lvalue", lvl + 2);
    node->lvalue()->accept(this, lvl + 4);
    closeTag("lvalue", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_function_declaration_node(zu::function_declaration_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("type", lvl + 2);
    processType(node->type(), lvl + 4);//FIXME
    closeTag("type", lvl + 2);
    openTag("id", lvl + 2);
    os() <<  std::string(lvl + 4, ' ') << *node->id() << std::endl;//FIXME
    closeTag("id", lvl + 2);
    openTag("args", lvl + 2);
    if(node->args() != nullptr)
        node->args()->accept(this, lvl + 4);
    closeTag("args", lvl + 2);
    openTag("literal", lvl + 2);
    if(node->literal() != nullptr)
        node->literal()->accept(this, lvl + 4);
    closeTag("literal", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_function_definition_node(zu::function_definition_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("type", lvl + 2);
    processType(node->type(), lvl + 4);//FIXME
    closeTag("type", lvl + 2);
    openTag("id", lvl + 2);
    os() <<  std::string(lvl + 4, ' ') << *node->id() << std::endl;//FIXME
    closeTag("id", lvl + 2);
    openTag("args", lvl + 2);
    if(node->args() != nullptr)
        node->args()->accept(this, lvl + 4);
    closeTag("args", lvl + 2);
    openTag("literal", lvl + 2);
    if(node->literal() != nullptr)
        node->literal()->accept(this, lvl + 4);
    closeTag("literal", lvl + 2);
    openTag("block", lvl + 2);
    if(node->block() != nullptr)
        node->block()->accept(this, lvl + 4);
    closeTag("block", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_function_call_node(zu::function_call_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("id", lvl + 2);
    os() <<  std::string(lvl + 4, ' ') << *node->id() << std::endl;//FIXME
    closeTag("id", lvl + 2);
    openTag("args", lvl + 2);
    if(node->args() != nullptr)
        node->args()->accept(this, lvl + 4);
    closeTag("args", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_break_node(zu::break_node * const node, int lvl) {
  os() << std::string(lvl, ' ') << "<" << node->name() << " />" << std::endl;//FIXME
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_continue_node(zu::continue_node * const node, int lvl) {
  os() << std::string(lvl, ' ') << "<" << node->name() << " />" << std::endl;//FIXME
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_return_node(zu::return_node * const node, int lvl) {
  os() << std::string(lvl, ' ') << "<" << node->name() << " />" << std::endl;//FIXME
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_memory_allocation_node(zu::memory_allocation_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("lvalue", lvl + 2);
    node->lvalue()->accept(this, lvl + 4);
    closeTag("lvalue", lvl + 2);
    openTag("size", lvl + 2);
    node->size()->accept(this, lvl + 4);
    closeTag("size", lvl + 2);
    closeTag(node, lvl);
}

//---------------------------------------------------------------------------

void zu::xml_writer::do_index_node(zu::index_node * const node, int lvl) {
    openTag(node, lvl);
    openTag("id", lvl + 2);
    node->id()->accept(this, lvl + 4);
    closeTag("id", lvl + 2);
    openTag("pos", lvl + 2);
    node->pos()->accept(this, lvl + 4);
    closeTag("pos", lvl + 2);
    closeTag(node, lvl);
}
