#!/bin/bash

LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
NC='\033[0m' # No Color

for filename in *.zu; do
    if [[ $(../project/zu $filename 2>&1) ]]; then
   		echo -e "${LIGHTRED}[FAIL]${NC} - $filename";
   	else
   		echo -e "${LIGHTGREEN}[OK]${NC} - $filename";
   	fi
done