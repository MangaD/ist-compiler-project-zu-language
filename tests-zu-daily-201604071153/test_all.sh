#!/bin/bash

LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
NC='\033[0m' # No Color

for file in *.zu; do
	../project/zu $file
	filename=$(basename $file)
	filename="${filename%.*}"
	yasm -felf32 $filename.asm
	ld -m elf_i386 -o $filename $filename.o -lrts
	./$filename > output/$filename.out
	rm $filename
    if [[ $(diff output/$filename.out expected/$filename.out 2>&1) ]]; then
   		echo -e "${LIGHTRED}[FAIL]${NC} - $filename";
   	else
   		echo -e "${LIGHTGREEN}[OK]${NC} - $filename";
   	fi
done

rm *.asm *.o