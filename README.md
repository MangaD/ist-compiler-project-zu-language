# IST - Compiler Project ("zu" language) #

This is a compiler project using the C++ programming language, lex and yacc. This project was done for the Compilers class at Instituto Superior T�cnico in the 2015-16 academic year. The code generation part of the compiler has not been finished.

The assignment details can be found [here](https://www.l2f.inesc-id.pt/~david/w/pt/Compiladores/Projecto_de_Compiladores) or at the folder "Enunciado", in Portuguese.


### Compiling ###

For compiling it is necessary to install the CDK and RTS libraries on your system, they are located at the "libraries" folder. Then simply run "make" on the "project" folder. The "project_bak" folder contains the basis of the compiler structure, provided by our teacher, which we then modified and completed, forming the "project" folder.


### Testing ###

For testing the syntactic analysis run the script "test_yacc.sh" located at the tests-zu-daily folder. For testing the syntactic analysis plus code generation run "test_all.sh".


### Credits ###

#### Developers ####

* David Matos (teacher)
* David Gon�alves
* Jo�o Monteiro